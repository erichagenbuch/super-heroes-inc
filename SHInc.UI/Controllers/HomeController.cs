﻿using System.Web.Mvc;

namespace SHInc.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Programs()
        {
            return View();
        }

        public ActionResult Calendar()
        {
            return View();
        }
    }
}
