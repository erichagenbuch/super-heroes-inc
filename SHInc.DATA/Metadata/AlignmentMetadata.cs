﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHInc.DATA
{
    [MetadataType(typeof(AlignmentMetadata))]
    public partial class Alignment { }

    public class AlignmentMetadata
    {

        public int AlignmentID { get; set; }
        [Display(Name ="Alignment Name")]
        public string AlignmentName { get; set; }
    }
}
