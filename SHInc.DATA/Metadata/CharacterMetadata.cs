﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHInc.DATA
{
    [MetadataType(typeof(CharacterMetadata))]
    public partial class Character { }

    public class CharacterMetadata
    {
        public int ID { get; set; }
        public string Alias { get; set; }
        [Display(Name ="Full Name")]
        public string FullName { get; set; }
        public int Alignment { get; set; }
        public string Origin { get; set; }

        public virtual Alignment Alignment1 { get; set; }
    }
}
